CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

 * For a full description of the module visit:
   https://www.drupal.org/project/editionguard_api

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/editionguard_api


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Install the EditionGuard API module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

1. Navigate to Administration > Extend and enable the module.
2. Navigate to Administration > Configuration > Web Services >
    EditionGuard API Settings. Provide settings values. Save configurations.

Configurable parameters:
 * Oauth email -> registered email with EditionGuard
 * Oauth password -> registered password  with EditionGuard
 * Token expire

USAGE
-------------
Check src/Form/TestForm for examples.


     // Edition Guard Client API.
    $client = \Drupal::service('editionguard_api.client');

     // Create ebook - https://editionguard.api-docs.io/v2/book .
    $form_params['title'] = 'MY_BOOK_TITLE_NAME';

    $fid = $entity->get("MY_FIELD_FILE_NAME")->entity->getFileUri();
    $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')
      ->getViaUri($fid);
    $file_path = $stream_wrapper_manager->realpath();
    $form_params['resource'] = $file_path;
    $endpoint = $client->getEndpointPluginManager()->createInstance('book_create');
    $result = $client->request($endpoint, $endpoint_query_params = [],
      $endpoint_form_params);

    // Get ebooks list - https://editionguard.api-docs.io/v2/book .
    $endpoint = $client->getEndpointPluginManager()->createInstance('book_list');
    $result = $client->request($endpoint, $endpoint_query_params = [],
      $endpoint_form_params = []);


MAINTAINERS
-----------

 * lexsoft - https://www.drupal.org/u/lexsoft
