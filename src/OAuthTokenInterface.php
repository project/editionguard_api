<?php

namespace Drupal\editionguard_api;

/**
 * Defines the interface for oauth token.
 *
 * @package Drupal\editionguard_api
 */
interface OAuthTokenInterface extends \Serializable {

  /**
   * Access token for the OAuth2 Authorization.
   *
   * @return string
   *   Access token.
   */
  public function accessToken(): string;

  /**
   * Refresh token for OAuth2 Authorization.
   *
   * @return string
   *   Refresh token.
   */
  public function refreshToken(): string;

  /**
   * Lifespan of the token in seconds.
   *
   * @return int
   *   Lifespan of the token in seconds.
   */
  public function lifespan(): int;

  /**
   * Timestamp of expiration.
   *
   * @return int
   *   Expiration unix timestamp.
   */
  public function expiresAt(): int;

  /**
   * Whether or not the token is expired.
   *
   * @return bool
   *   True if expiresAt() is greater than now, otherwise false.
   */
  public function isExpired(): bool;

}
