<?php

namespace Drupal\editionguard_api\Exception;

/**
 * Defines a custom exception if endpoint required options are missing.
 *
 * @package Drupal\editionguard_api\Exception
 */
class EndpointRequiredOptionsMissing extends \RuntimeException {}
