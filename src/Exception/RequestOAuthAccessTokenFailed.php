<?php

namespace Drupal\editionguard_api\Exception;

/**
 * Defines a custom exception if the request for token failed.
 *
 * @package Drupal\editionguard_api\Exception
 */
class RequestOAuthAccessTokenFailed extends \RuntimeException {}
