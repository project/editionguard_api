<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Returns all the attributes for the specified title.
 *
 * @Endpoint(
 *   id = "book_get",
 *   name = @Translation("Book Get"),
 *   path = "book/[id]",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   requiredQueryParams = {
 *     "id",
 *   },
 * )
 */
class BookGet extends EndpointPluginBase {}
