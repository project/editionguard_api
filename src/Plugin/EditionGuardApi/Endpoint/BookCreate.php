<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Upload a new book into EditionGuard.
 *
 * @Endpoint(
 *   id = "book_create",
 *   name = @Translation("Book Create"),
 *   path = "book",
 *   method = "POST",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   requiredFormParams = {
 *     "title",
 *     "resource",
 *   },
 *   defaultRequestFormParams = {
 *     "author" = "",
 *     "publisher" = "",
 *   },
 * )
 */
class BookCreate extends EndpointPluginBase {}
