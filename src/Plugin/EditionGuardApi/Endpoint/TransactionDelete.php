<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Delete transaction.
 *
 * @Endpoint(
 *   id = "transaction_delete",
 *   name = @Translation("Transaction Delete"),
 *   path = "transaction/[transaction_id]",
 *   method = "DELETE",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/transaction",
 *   requiredQueryParams = {
 *     "transaction_id",
 *   },
 * )
 */
class TransactionDelete extends EndpointPluginBase {}
