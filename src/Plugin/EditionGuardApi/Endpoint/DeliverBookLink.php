<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Emails the provided address with a download link for the eBook.
 *
 * @Endpoint(
 *   id = "deliver_book_link",
 *   name = @Translation("Deliver Book Link"),
 *   path = "deliver-book-link",
 *   method = "POST",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/deliver-book-link",
 *   requiredFormParams = {
 *     "resource_id",
 *     "email",
 *   },
 *   defaultRequestFormParams = {
 *     "full_name" = "",
 *     "reply_to" = "",
 *   },
 * )
 */
class DeliverBookLink extends EndpointPluginBase {}
