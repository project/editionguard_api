<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Deletes a master link, effectively rendering it unusable.
 *
 * @Endpoint(
 *   id = "master_link_delete",
 *   name = @Translation("Master Link Delete"),
 *   path = "master_link/[id]",
 *   method = "DELETE",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/master_link",
 *   requiredQueryParams = {
 *     "id",
 *   },
 * )
 */
class MasterLinkDelete extends EndpointPluginBase {}
