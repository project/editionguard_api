<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Create a list of transactions for all eBooks.
 *
 * @Endpoint(
 *   id = "transaction_list",
 *   name = @Translation("Transaction List"),
 *   path = "transaction",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/transaction",
 *   defaultRequestQueryParams = {
 *     "ordering" = "-created",
 *     "search": "",
 *     "page_size": "",
 *     "page": "",
 *     "start_date": "",
 *     "end_date": "",
 *     "resource_id": "",
 *     "show_instructions": "",
 *     "is_fulfilled": "",
 *   }
 * )
 */
class TransactionList extends EndpointPluginBase {}
