<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Delete book.
 *
 * @Endpoint(
 *   id = "book_delete",
 *   name = @Translation("Book Delete"),
 *   path = "book/[id]",
 *   method = "DELETE",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   requiredQueryParams = {
 *     "id",
 *   },
 * )
 */
class BookDelete extends EndpointPluginBase {}
