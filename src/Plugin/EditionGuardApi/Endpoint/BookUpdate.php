<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Updates the specified eBook by setting the values of the parameters passed.
 *
 * @Endpoint(
 *   id = "book_update",
 *   name = @Translation("Book Update"),
 *   path = "book/[id]",
 *   method = "PATCH",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   requiredQueryParams = {
 *     "id",
 *   },
 *   requiredFormParams = {
 *     "resource",
 *   },
 *   defaultRequestFormParams = {
 *     "author" = "",
 *     "publisher" = "",
 *   },
 * )
 */
class BookUpdate extends EndpointPluginBase {}
