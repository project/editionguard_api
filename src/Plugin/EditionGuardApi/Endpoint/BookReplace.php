<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Any books downloaded prior will not have their settings changed.
 *
 * @Endpoint(
 *   id = "book_replace",
 *   name = @Translation("Book Replace"),
 *   path = "book/[id]",
 *   method = "PUT",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   requiredQueryParams = {
 *     "id",
 *   },
 *   requiredFormParams = {
 *     "title",
 *     "resource",
 *   },
 *   defaultRequestFormParams = {
 *     "author" = "",
 *     "publisher" = "",
 *   },
 * )
 */
class BookReplace extends EndpointPluginBase {}
