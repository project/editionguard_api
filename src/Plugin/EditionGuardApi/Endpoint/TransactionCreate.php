<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Create a new transaction for a specific eBook called via its resource_id.
 *
 * @Endpoint(
 *   id = "transaction_create",
 *   name = @Translation("Transaction Create"),
 *   path = "transaction",
 *   method = "POST",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/transaction",
 *   requiredFormParams = {
 *     "resource_id",
 *   },
 *   defaultRequestFormParams = {
 *     "show_instructions": "",
 *     "external_id": "",
 *   },
 * )
 */
class TransactionCreate extends EndpointPluginBase {}
