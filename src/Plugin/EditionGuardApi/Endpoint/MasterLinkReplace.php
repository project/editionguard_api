<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Updates a master link. This endpoint is only for Adobe DRM protected e-books.
 *
 * @Endpoint(
 *   id = "master_link_replace",
 *   name = @Translation("Master Link Replace"),
 *   path = "master_link/[id]",
 *   method = "PUT",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/master_link",
 *   requiredQueryParams = {
 *     "id",
 *   },
 *   requiredFormParams = {
 *     "resource_id",
 *   },
 *   defaultRequestFormParams = {
 *     "show_instructions" = "",
 *     "uses_remaining" = "",
 *     "expiration_date" = "",
 *     "external_id" = "",
 *   },
 * )
 */
class MasterLinkReplace extends EndpointPluginBase {}
