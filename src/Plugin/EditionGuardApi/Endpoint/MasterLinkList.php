<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Get a list of all master links on an account.
 *
 * @Endpoint(
 *   id = "master_link_list",
 *   name = @Translation("Master Link List"),
 *   path = "master_link",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/master_link",
 * )
 */
class MasterLinkList extends EndpointPluginBase {}
