<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Partially updates a master link.
 *
 * @Endpoint(
 *   id = "master_link_update",
 *   name = @Translation("Master Link Update"),
 *   path = "master_link/[id]",
 *   method = "PATCH",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/master_link",
 *   requiredQueryParams = {
 *     "id",
 *   },
 *   requiredFormParams = {
 *     "resource_id",
 *   },
 *   defaultRequestFormParams = {
 *     "show_instructions" = "",
 *     "uses_remaining" = "",
 *     "expiration_date" = "",
 *     "external_id" = "",
 *   },
 * )
 */
class MasterLinkUpdate extends EndpointPluginBase {}
