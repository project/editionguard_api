<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Generate download links for a specific title.
 *
 * @Endpoint(
 *   id = "book_generate_links",
 *   name = @Translation("Book Generate Links"),
 *   path = "book/[id]/generate_links",
 *   method = "POST",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   requiredQueryParams = {
 *     "id",
 *   },
 *   requiredFormParams = {
 *     "links_count"
 *   },
 * )
 */
class BookGenerateLinks extends EndpointPluginBase {}
