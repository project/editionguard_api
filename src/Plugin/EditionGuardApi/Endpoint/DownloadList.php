<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * This endpoint returns all eBooks fulfilled through their download link.
 *
 * @Endpoint(
 *   id = "download_list",
 *   name = @Translation("Download List"),
 *   path = "download",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/download",
 *   defaultRequestQueryParams = {
 *     "ordering" = "-created",
 *     "start_date" = "",
 *     "end_date" = "",
 *     "search" = "",
 *     "resource_id" = "",
 *     "transaction_id" = "",
 *     "page_size" = "",
 *     "page" = "",
 *   }
 * )
 */
class DownloadList extends EndpointPluginBase {}
