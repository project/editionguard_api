<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Return the list of books including the type of DRM.
 *
 * @Endpoint(
 *   id = "book_list",
 *   name = @Translation("Book List"),
 *   path = "book",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/book",
 *   defaultRequestQueryParams = {
 *     "is_active" = "True",
 *     "status" = "ready",
 *     "ordering" = "-created",
 *   }
 * )
 */
class BookList extends EndpointPluginBase {}
