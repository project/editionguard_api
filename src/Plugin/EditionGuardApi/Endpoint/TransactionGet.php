<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Returns the specified transaction.
 *
 * @Endpoint(
 *   id = "transaction_get",
 *   name = @Translation("Transaction Get"),
 *   path = "transaction/[transaction_id]",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/transaction",
 *   requiredQueryParams = {
 *     "transaction_id",
 *   },
 * )
 */
class TransactionGet extends EndpointPluginBase {}
