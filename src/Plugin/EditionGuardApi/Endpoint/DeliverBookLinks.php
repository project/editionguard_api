<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Emails the provided address with a download links for the eBook.
 *
 * @Endpoint(
 *   id = "deliver_book_links",
 *   name = @Translation("Deliver Book Links"),
 *   path = "deliver-book-links",
 *   method = "POST",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/deliver-book-link",
 *   requiredFormParams = {
 *     "book_list",
 *     "email",
 *   },
 *   defaultRequestFormParams = {
 *     "full_name" = "",
 *     "reply_to" = "",
 *   },
 * )
 */
class DeliverBookLinks extends EndpointPluginBase {}
