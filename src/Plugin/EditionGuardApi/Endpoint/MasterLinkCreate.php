<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Create a new master link for a specific eBook called via its resource_id.
 *
 * @Endpoint(
 *   id = "master_link_create",
 *   name = @Translation("Master Link Create"),
 *   path = "master_link",
 *   method = "POST",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/master_link",
 *   requiredFormParams = {
 *     "resource_id",
 *   },
 *   defaultRequestFormParams = {
 *     "show_instructions" = "",
 *     "uses_remaining" = "",
 *     "expiration_date" = "",
 *     "external_id" = "",
 *   },
 * )
 */
class MasterLinkCreate extends EndpointPluginBase {}
