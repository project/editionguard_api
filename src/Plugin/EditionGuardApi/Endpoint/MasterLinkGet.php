<?php

namespace Drupal\editionguard_api\Plugin\EditionGuardApi\Endpoint;

use Drupal\editionguard_api\EndpointPluginBase;

/**
 * Get details of a master link.
 *
 * @Endpoint(
 *   id = "master_link_get",
 *   name = @Translation("Master Link Get"),
 *   path = "master_link/[id]",
 *   documentationUrl= "https://editionguard.api-docs.io/v2/master_link",
 *   requiredQueryParams = {
 *     "id",
 *   },
 * )
 */
class MasterLinkGet extends EndpointPluginBase {}
