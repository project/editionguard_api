<?php

namespace Drupal\editionguard_api;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\editionguard_api\Exception\EndpointRequiredOptionsMissing;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a base class for API endpoints.
 *
 * @package Drupal\editionguard_api
 */
abstract class EndpointPluginBase extends PluginBase implements EndpointPluginInterface {

  /**
   * Module config values.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $editionGuardApiConfig;

  /**
   * Endpoint request options.
   *
   * @var array
   */
  protected $requestQueryParams = [];

  /**
   * Endpoint request form data.
   *
   * @var array
   */
  protected $requestFormParams = [];

  /**
   * EndpointPluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->editionGuardApiConfig = $config_factory->get('editionguard_api.settings');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPluginName() {
    return $this->pluginDefinition['name'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredQueryParams(): array {
    return $this->pluginDefinition['requiredQueryParams'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredFormParams(): array {
    return $this->pluginDefinition['requiredFormParams'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRequestQueryParams(): array {
    return $this->pluginDefinition['defaultRequestQueryParams'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultRequestFormParams(): array {
    return $this->pluginDefinition['defaultRequestFormParams'] ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestPath(): string {
    $replacements = [];
    $options = $this->getRequestQueryParams();
    foreach ($this->getRequiredQueryParams() as $required_key) {
      if (!isset($options[$required_key])) {
        throw new EndpointRequiredOptionsMissing($required_key);
      }
      $replacements["[{$required_key}]"] = $options[$required_key];
    }
    return strtr($this->pluginDefinition['path'], $replacements);
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestMethod(): string {
    return $this->pluginDefinition['method'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestAuthType(): string {
    return $this->pluginDefinition['authType'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestQueryParams(): array {
    return $this->requestQueryParams;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequestFormParams(): array {
    return $this->requestFormParams;
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestQueryParams(array $query_params = []) {
    $query_params = array_replace($this->getDefaultRequestQueryParams(), $query_params);

    $this->requestQueryParams = array_filter($query_params, function ($value) {
      return $value !== '' && $value !== [];
    });
  }

  /**
   * {@inheritdoc}
   */
  public function setRequestFormParams(array $form_params = []) {
    $form_params = array_replace($this->getDefaultRequestFormParams(), $form_params);

    $this->requestFormParams = $form_params;
  }

  /**
   * {@inheritdoc}
   */
  public function canPerformRequest(): bool {
    $options = $this->getRequestQueryParams();
    $data = $this->getRequestFormParams();
    foreach ($this->getRequiredQueryParams() as $required_key) {
      if (!isset($options[$required_key])) {
        return FALSE;
      }
    }
    foreach ($this->getRequiredFormParams() as $required_key) {
      if (!isset($data[$required_key])) {
        return FALSE;
      }
    }

    return TRUE;
  }

}
