<?php

namespace Drupal\editionguard_api;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Defines the interface for API endpoints.
 *
 * @package Drupal\editionguard_api
 */
interface EndpointPluginInterface extends PluginInspectionInterface, ContainerFactoryPluginInterface {

  /**
   * Endpoint authorization type indicating api_key required for requests.
   */
  const ENDPOINT_AUTH_TYPE_KEY = 'key';

  /**
   * Endpoint authorization type indicating oauth tokens required for requests.
   */
  const ENDPOINT_AUTH_TYPE_OAUTH = 'oauth';

  /**
   * Human readable plugin name.
   *
   * @return string
   *   Human readable plugin name.
   */
  public function getPluginName();

  /**
   * Get the prepared request path.
   *
   * @return string
   *   Endpoint request path.
   */
  public function getRequestPath(): string;

  /**
   * Get the method for the endpoint request.
   *
   * @return string
   *   HTTP request method like: GET, POST, etc.
   */
  public function getRequestMethod(): string;

  /**
   * Get the endpoint's authorization type.
   *
   * @return string
   *   Endpoint authorization type.
   */
  public function getRequestAuthType(): string;

  /**
   * Get array of required request query parameter keys.
   *
   * @return array
   *   Array of required request query parameter keys.
   */
  public function getRequiredQueryParams(): array;

  /**
   * Get array of required request form parameter keys.
   *
   * @return array
   *   Array of required request form parameter keys.
   */
  public function getRequiredFormParams(): array;

  /**
   * Get array of default request query parameters.
   *
   * @return array
   *   Default endpoint request query parameters.
   */
  public function getDefaultRequestQueryParams(): array;

  /**
   * Get array of default request form parameters.
   *
   * @return array
   *   Default endpoint request form parameters.
   */
  public function getDefaultRequestFormParams(): array;

  /**
   * Get populated request query parameters.
   *
   * @return array
   *   Request specific query parameters.
   */
  public function getRequestQueryParams(): array;

  /**
   * Get populated request form parameters.
   *
   * @return array
   *   Request specific form parameters.
   */
  public function getRequestFormParams(): array;

  /**
   * Populate the request query parameters.
   *
   * @param array $query_params
   *   Request specific query parameters.
   */
  public function setRequestQueryParams(array $query_params = []);

  /**
   * Populate the request form parameters.
   *
   * @param array $form_params
   *   Request specific form parameters.
   */
  public function setRequestFormParams(array $form_params = []);

  /**
   * Whether or not the endpoint can perform the request in its current state.
   *
   * @return bool
   *   True if the request is ready to be performed.
   */
  public function canPerformRequest(): bool;

}
