<?php

namespace Drupal\editionguard_api;

/**
 * Defines the interface for EditionGuard API Client.
 *
 * @package Drupal\editionguard_api\Service\EditionGuard
 */
interface EditionGuardApiClientInterface {

  /**
   * Get the http client.
   *
   * @return \GuzzleHttp\Client
   *   Http client.
   */
  public function getHttpClient();

  /**
   * Get the plugin manager for endpoints.
   *
   * @return \Drupal\editionguard_api\EndpointPluginManager
   *   Endpoint plugin manager.
   */
  public function getEndpointPluginManager();

  /**
   * Perform a request to editionguard API.
   *
   * @param \Drupal\editionguard_api\EndpointPluginInterface $endpoint
   *   Endpoint plugin instance.
   * @param array $query_params
   *   Array of endpoint query parameters for the request.
   * @param array $form_params
   *   Array of endpoint form parameters for the request.
   *
   * @return array
   *   Json response decoded as array.
   */
  public function request(EndpointPluginInterface $endpoint, array $query_params = [], array $form_params = []);

  /**
   * Whether or not the client can authorize for private requests.
   *
   * @return bool
   *   True if client has required secrets, false otherwise.
   */
  public function canAuthorizePrivate(): bool;

}
