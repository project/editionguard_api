<?php

namespace Drupal\editionguard_api;

/**
 * Defines the oauth token.
 *
 * @package Drupal\editionguard_api
 */
class OAuthToken implements OAuthTokenInterface {

  /**
   * Access token.
   *
   * @var string
   */
  protected $accessToken = '';

  /**
   * Refresh token.
   *
   * @var string
   */
  protected $refreshToken = '';

  /**
   * Lifespan of the token in seconds.
   *
   * @var int
   */
  protected $lifespan = 0;

  /**
   * Timestamp for when the token will be expired.
   *
   * @var int
   */
  protected $expiresAt = 0;

  /**
   * OAuthToken constructor.
   *
   * @param string $access_token
   *   Access token.
   * @param string $refresh_token
   *   Refresh token.
   * @param int $token_lifespan
   *   Lifespan of the token in seconds.
   */
  public function __construct(string $access_token, string $refresh_token, int $token_lifespan) {
    $this->accessToken = $access_token;
    $this->refreshToken = $refresh_token;
    $this->lifespan = $token_lifespan;
    $this->expiresAt = time() + $token_lifespan;
  }

  /**
   * {@inheritdoc}
   */
  public function serialize() {
    return \serialize([
      $this->accessToken,
      $this->refreshToken,
      $this->lifespan,
      $this->expiresAt,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function unserialize($serialized) {
    [
      $this->accessToken,
      $this->refreshToken,
      $this->lifespan,
      $this->expiresAt,
    ] = \unserialize($serialized, ['allowed_classes' => FALSE]);
  }

  /**
   * {@inheritdoc}
   */
  public function accessToken(): string {
    return $this->accessToken;
  }

  /**
   * {@inheritdoc}
   */
  public function refreshToken(): string {
    return $this->refreshToken;
  }

  /**
   * {@inheritdoc}
   */
  public function lifespan(): int {
    return $this->lifespan;
  }

  /**
   * {@inheritdoc}
   */
  public function expiresAt(): int {
    return $this->expiresAt;
  }

  /**
   * {@inheritdoc}
   */
  public function isExpired(): bool {
    // Consider the token expired 10 minutes early.
    return (time() + 600) >= $this->expiresAt();
  }

}
