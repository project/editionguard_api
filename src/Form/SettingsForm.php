<?php

namespace Drupal\editionguard_api\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the settings form for EditionGuard API credentials.
 *
 * @package Drupal\editionguard_api\Form
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editionguard_api_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'editionguard_api.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('editionguard_api.settings');

    $form['api'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Client Settings'),
      'oauth_email' => [
        '#type' => 'textfield',
        '#title' => $this->t('OAuth Email'),
        '#description' => $this->t('Your EditionGuard b2b login email. Required to use private endpoints. <a href="https://developers.editionguard.com/authentication#password">Documentation</a>'),
        '#default_value' => $config->get('oauth_email'),
      ],
      'oauth_password' => [
        '#type' => 'password',
        '#title' => $this->t('OAuth Password'),
        '#description' => $this->t('Your EditionGuard b2b login password. Required to use private endpoints. Recommended this value be kept in a settings*.php file and not stored in config. <pre>@example</pre>', [
          '@example' => '$config["editionguard_api.settings"]["oauth_password"] = "";',
        ]),
        '#default_value' => $config->get('oauth_password'),
      ],
      'token_expire' => [
        '#type' => 'textfield',
        '#title' => $this->t('Token Expire'),
        '#description' => $this->t('By default, access tokens are valid for 7 days. Refresh tokens are used to get a new access token when your current access token expires.'),
        '#default_value' => $config->get('token_expire') ?: '604800',
      ],
      'logging_enabled' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Enable logging of API calls.'),
        '#description' => $this->t('Record the effective URL and other details about performed API calls to Drupal logs.'),
        '#default_value' => $config->get('logging_enabled'),
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('editionguard_api.settings')
      ->set('oauth_email', $form_state->getValue('oauth_email'))
      ->set('oauth_password', $form_state->getValue('oauth_password'))
      ->set('token_expire', $form_state->getValue('token_expire'))
      ->set('logging_enabled', $form_state->getValue('logging_enabled'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
