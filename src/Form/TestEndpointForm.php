<?php

namespace Drupal\editionguard_api\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the test form for EditionGuard API endpoints.
 *
 * @package Drupal\editionguard_api\Form
 */
class TestEndpointForm extends FormBase {

  /**
   * EditionGuard Api client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $client;

  /**
   * TestForm constructor.
   *
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api_client
   *   EditionGuard Api client.
   */
  public function __construct(EditionGuardApiClientInterface $editionguard_api_client) {
    $this->client = $editionguard_api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('editionguard_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editionguard_api_test_endpoint';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $endpoint_id = $this->getRequest()->get('endpoint_id');
    try {
      $endpoint = $this->client->getEndpointPluginManager()->createInstance($endpoint_id);
    }
    catch (PluginNotFoundException $exception) {
      $this->messenger()->addError($this->t('Endpoint ID Not Found - :endpoint_id', [':endpoint_id' => $endpoint_id]));
      return [];
    }

    $documentation_url = $endpoint->getPluginDefinition()['documentationUrl'];
    $form['endpoint_id'] = [
      '#type' => 'value',
      '#value' => $endpoint_id,
    ];
    $form['endpoint_params'] = [
      '#type' => 'fieldset',
      '#title' => $endpoint->getPluginName(),
      '#description' => Markup::create("<a href='{$documentation_url}'>{$documentation_url}</a>"),
      '#tree' => TRUE,
    ];
    $endpoint->setRequestQueryParams();
    $endpoint->setRequestFormParams();
    foreach ($endpoint->getRequiredQueryParams() as $default_value) {
      $form['endpoint_params']['endpoint_query_params'][$default_value] = [
        '#type' => 'textfield',
        '#title' => $default_value,
        '#default_value' => '',
        '#required' => TRUE,
      ];
    }
    foreach ($endpoint->getRequiredFormParams() as $default_value) {
      $form['endpoint_params']['endpoint_form_params'][$default_value] = [
        '#type' => 'textfield',
        '#title' => $default_value,
        '#default_value' => '',
        '#required' => TRUE,
      ];
    }
    foreach ($endpoint->getDefaultRequestQueryParams() as $param => $default_value) {
      $form['endpoint_params']['endpoint_query_params'][$param] = [
        '#type' => 'textfield',
        '#title' => $param,
        '#default_value' => is_array($default_value) ? implode(',', $default_value) : $default_value,
      ];
    }
    foreach ($endpoint->getDefaultRequestFormParams() as $param => $default_value) {
      $form['endpoint_params']['endpoint_form_params'][$param] = [
        '#type' => 'textfield',
        '#title' => $param,
        '#default_value' => is_array($default_value) ? implode(',', $default_value) : $default_value,
      ];
    }

    $form['actions']['#type'] = 'actions';
    $form['actions']['test_endpoints'] = [
      '#type' => 'submit',
      '#value' => $this->t('Perform Test'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $messenger = $this->messenger();
    $endpoint_id = $form_state->getValue('endpoint_id');
    $endpoint_params = $form_state->getValue('endpoint_params');
    $endpoint_query_params = $endpoint_params['endpoint_query_params'] ?? [];
    $endpoint_form_params = $endpoint_params['endpoint_form_params'] ?? [];
    try {
      $endpoint = $this->client->getEndpointPluginManager()->createInstance($endpoint_id);
    }
    catch (PluginNotFoundException $exception) {
      $messenger->addError($this->t('Endpoint ID Not Found - :endpoint_id', [':endpoint_id' => $endpoint_id]));
      return;
    }
    $result = $this->client->request($endpoint, $endpoint_query_params, $endpoint_form_params);
    $messenger->addStatus($this->t(':endpoint_id - :result', [
      ':endpoint_id' => $endpoint_id,
      ':result' => empty($result) ? $this->t('Failed') : $this->t('Success'),
    ]));
    $messenger->addStatus($this->arrayToSafeMarkup($result));
  }

  /**
   * Convert array to safe markup for formatted messages.
   *
   * @param array $array
   *   Array to convert to markup.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string
   *   Markup string.
   */
  protected function arrayToSafeMarkup(array $array) {
    return Markup::create(Xss::filterAdmin('<pre>' . print_r($array, 1) . '</pre>'));
  }

}
