<?php

namespace Drupal\editionguard_api\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\editionguard_api\EditionGuardApiClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides the test form for EditionGuard API.
 *
 * @package Drupal\editionguard_api\Form
 */
class TestForm extends FormBase {

  /**
   * EditionGuard Api client.
   *
   * @var \Drupal\editionguard_api\EditionGuardApiClientInterface
   */
  protected $client;

  /**
   * TestForm constructor.
   *
   * @param \Drupal\editionguard_api\EditionGuardApiClientInterface $editionguard_api_client
   *   EditionGuard Api client.
   */
  public function __construct(EditionGuardApiClientInterface $editionguard_api_client) {
    $this->client = $editionguard_api_client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('editionguard_api.client')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'editionguard_api_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $private_ready = $this->client->canAuthorizePrivate();
    $endpoints = $this->client->getEndpointPluginManager()->getDefinitions();
    $endpoint_headers = [
      'id' => $this->t('ID'),
      'name' => $this->t('Name'),
      'provider' => $this->t('Provider'),
      'path' => $this->t('Path'),
      'method' => $this->t('Method'),
      'auth' => $this->t('Auth Type'),
      'required_query' => $this->t('Required Query Params'),
      'required_form' => $this->t('Required Form Params'),
      'test' => $this->t('Test'),
    ];

    $endpoint_rows = [];
    foreach ($endpoints as $plugin_id => $plugin_definition) {
      $test_url = Url::fromRoute('editionguard_api.test_endpoint', ['endpoint_id' => $plugin_id])->toString();
      $private_ready_color = ($plugin_definition['authType'] == 'oauth' && !$private_ready) ? 'red' : 'inherit';
      $endpoint_rows[$plugin_id] = [
        'id' => $plugin_id,
        'name' => Markup::create("<a href='{$plugin_definition['documentationUrl']}'>{$plugin_definition['name']}</a>"),
        'provider' => $plugin_definition['provider'],
        'path' => $plugin_definition['path'],
        'method' => $plugin_definition['method'],
        'auth' => Markup::create("<span style='color: {$private_ready_color};'>{$plugin_definition['authType']}</span>"),
        'required_query' => implode(', ', $plugin_definition['requiredQueryParams'] ?? []),
        'required_form' => implode(', ', $plugin_definition['requiredFormParams'] ?? []),
        'test' => Markup::create("<a href='$test_url'>{$endpoint_headers['test']}</a>"),
      ];
    }

    $form['endpoints'] = [
      'title' => [
        '#type' => 'html_tag',
        '#tag' => 'h2',
        '#value' => $this->t('Endpoints'),
      ],
      'selected_endpoints_to_test' => [
        '#type' => 'tableselect',
        '#header' => $endpoint_headers,
        '#options' => $endpoint_rows,
        '#empty' => $this->t('No endpoint plugins found.'),
      ],
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['test_endpoints'] = [
      '#type' => 'submit',
      '#value' => $this->t('Quick Test Endpoints'),
      '#button_type' => 'primary',
    ];
    $form['actions']['test_oauth'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test OAuth Connection'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    $trigger_button = 'submit';
    if (isset($trigger['#parents'], $trigger['#parents'][0])) {
      $trigger_button = $trigger['#parents'][0];
    }

    switch ($trigger_button) {
      case 'test_oauth':
        $this->actionTestOauth($form_state);
        break;

      case 'test_endpoints':
      default:
        $this->actionTestEndpoints($form_state);
    }
  }

  /**
   * Test an endpoint that requires OAuth.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Submitted form state.
   */
  protected function actionTestOauth(FormStateInterface $form_state) {
    $messenger = $this->messenger();
    $endpoint = $this->client->getEndpointPluginManager()->createInstance('book_list');

    $result = $this->client->request($endpoint);
    $messenger->addStatus($this->t(':endpoint_id - :result', [
      ':endpoint_id' => $endpoint->getPluginId(),
      ':result' => empty($result) ? $this->t('Failed') : $this->t('Success'),
    ]));
  }

  /**
   * Default submit action performs endpoint tests.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Submitted form state.
   */
  protected function actionTestEndpoints(FormStateInterface $form_state) {
    $messenger = $this->messenger();
    $endpoint_ids = array_filter($form_state->getValue('selected_endpoints_to_test'));

    foreach ($endpoint_ids as $endpoint_id) {
      $endpoint = $this->client->getEndpointPluginManager()->createInstance($endpoint_id);
      $endpoint->setRequestQueryParams();
      if ($endpoint->canPerformRequest()) {
        $result = $this->client->request($endpoint);
        $messenger->addStatus($this->t(':endpoint_id - :result', [
          ':endpoint_id' => $endpoint_id,
          ':result' => empty($result) ? $this->t('Failed') : $this->t('Success'),
        ]));
        usleep(250);
      }
      else {
        $messenger->addWarning($this->t(':endpoint_id - Cannot be tested without additional required parameters.', [
          ':endpoint_id' => $endpoint_id,
        ]));
      }
    }
  }

  /**
   * Convert array to safe markup for formatted messages.
   *
   * @param array $array
   *   Array to convert to markup.
   *
   * @return \Drupal\Component\Render\MarkupInterface|string
   *   Markup string.
   */
  protected function arrayToSafeMarkup(array $array) {
    return Markup::create('<pre>' . print_r($array, 1) . '</pre>');
  }

}
