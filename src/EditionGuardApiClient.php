<?php

namespace Drupal\editionguard_api;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\editionguard_api\Exception\RequestOAuthAccessTokenFailed;

/**
 * Defines the EditionGuard API Client.
 *
 * @package Drupal\editionguard_api\Service
 */
class EditionGuardApiClient implements EditionGuardApiClientInterface {

  /**
   * Module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * API client instance.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Endpoint plugin manager.
   *
   * @var \Drupal\editionguard_api\EndpointPluginManager
   */
  protected $endpointPluginManager;

  /**
   * Json service.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected $json;

  /**
   * Logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Default cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Http client options.
   *
   * @var array
   */
  protected $clientOptions = [];

  /**
   * API version number.
   *
   * @var string
   */
  protected $version = 'v2';

  /**
   * Client constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   Drupal core config factory.
   * @param \Drupal\Core\Http\ClientFactory $http_client_factory
   *   Drupal Guzzle HTTP Client factory.
   * @param \Drupal\editionguard_api\EndpointPluginManager $endpoint_plugin_manager
   *   Endpoint plugin manager.
   * @param \Drupal\Component\Serialization\Json $json
   *   Json service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_channel_factory
   *   Logger channel factory service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   */
  public function __construct(ConfigFactory $config_factory, ClientFactory $http_client_factory, EndpointPluginManager $endpoint_plugin_manager, Json $json, LoggerChannelFactoryInterface $logger_channel_factory, CacheBackendInterface $cache_backend) {
    $this->config = $config_factory->get('editionguard_api.settings');
    $this->clientOptions = [
      'base_uri' => "https://app.editionguard.com/api/{$this->version}/",
    ];
    $this->httpClient = $http_client_factory->fromOptions($this->clientOptions);
    $this->endpointPluginManager = $endpoint_plugin_manager;
    $this->json = $json;
    $this->logger = $logger_channel_factory->get('editionguard_api');
    $this->cache = $cache_backend;
  }

  /**
   * {@inheritdoc}
   */
  public function getHttpClient() {
    return $this->httpClient;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpointPluginManager() {
    return $this->endpointPluginManager;
  }

  /**
   * {@inheritdoc}
   */
  public function request(EndpointPluginInterface $endpoint, array $query_params = [], array $form_params = []) {
    $endpoint->setRequestQueryParams($query_params);
    $endpoint->setRequestFormParams($form_params);
    $query_params = $endpoint->getRequestQueryParams();
    $form_params = $endpoint->getRequestFormParams();
    $body_params = 'form_params';

    if ($endpoint->getRequestAuthType() == EndpointPluginInterface::ENDPOINT_AUTH_TYPE_OAUTH) {
      $token = $this->getAccessToken();
      $authorization = $token->accessToken();
    }

    // To upload the ebook we need to POST as multipart.
    if ($endpoint->getPluginId() === 'book_create' || $endpoint->getPluginId() === 'book_update' || $endpoint->getPluginId() === 'book_replace') {
      $body_params = 'multipart';
      $multi_params = [];
      foreach ($form_params as $key => $value) {
        if ($key === 'resource') {
          $multi_params[] = [
            'name' => $key,
            'contents' => fopen($value, 'r'),
          ];
        }
        else {
          $multi_params[] = [
            'name' => $key,
            'contents' => $value,
          ];
        }
      }
      $form_params = $multi_params;
    }
    $response = $this->getHttpClient()
      ->request($endpoint->getRequestMethod(), $endpoint->getRequestPath(), [
        'http_errors' => FALSE,
        'headers' => [
          'authorization' => 'Token ' . $authorization,
        ],
        'query' => $query_params,
        $body_params => $form_params,
      ]);
    if ($this->config->get('logging_enabled')) {
      $this->logger->info('Request to %endpoint_id, Status %status ' . '<pre><code>' . print_r($response, TRUE) . '</code></pre>', [
        '%endpoint_id' => $endpoint->getPluginId(),
        '%status' => $response->getStatusCode(),
      ]);
    }
    if ($response->getStatusCode() === 200 || $response->getStatusCode() === 201) {
      if ($this->config->get('logging_enabled')) {
        $this->logger->info('Response: ' . '<pre><code>' . print_r($this->json::decode($response->getBody()), TRUE) . '</code></pre>');
      }
      return $this->json::decode($response->getBody());
    }

    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function canAuthorizePrivate(): bool {
    return ($this->config->get('oauth_email') && $this->config->get('oauth_password'));
  }

  /**
   * Save an access token to the cache backend.
   *
   * @param \Drupal\editionguard_api\OAuthTokenInterface $token
   *   Cached access token instance.
   */
  protected function saveAccessToken(OAuthTokenInterface $token) {
    $this->cache->set('editionguard_api.oauth_access_token', $token, $token->expiresAt());
  }

  /**
   * Retrieve a valid access token.
   *
   * @return \Drupal\editionguard_api\OAuthTokenInterface
   *   Valid access token instance.
   */
  protected function getAccessToken() {
    $cache = $this->cache->get('editionguard_api.oauth_access_token', TRUE);
    /** @var \Drupal\editionguard_api\OAuthTokenInterface $token */
    if ($cache) {
      $token = $cache->data;
    }

    // If no existing previous token, request a new one.
    if (!isset($token)) {
      $token = $this->requestNewAccessToken();
      $this->saveAccessToken($token);
    }

    // Token is expired, refresh it.
    if ($token->isExpired()) {
      $token = $this->refreshAccessToken($token);
      $this->saveAccessToken($token);
    }

    return $token;
  }

  /**
   * Request a new OAuth access token from EditionGuard.
   *
   * @return \Drupal\editionguard_api\OAuthToken
   *   Newly instantiated access token.
   */
  protected function requestNewAccessToken() {
    $response = $this->requestAccessToken();

    if ($response->getStatusCode() === 200) {
      $values = $this->json::decode($response->getBody());
      $values['access_token'] = $values['token'];
      $values['refresh_token'] = 'refresh';
      $values['expires_in'] = $this->config->get('token_expire');
      return new OAuthToken($values['access_token'], $values['refresh_token'], $values['expires_in']);
    }

    throw new RequestOAuthAccessTokenFailed();
  }

  /**
   * Refresh an existing access token.
   *
   * @param \Drupal\editionguard_api\OAuthTokenInterface $token
   *   Existing access token with a refreshToken value.
   *
   * @return \Drupal\editionguard_api\OAuthToken
   *   Newly instantiated access token.
   */
  protected function refreshAccessToken(OAuthTokenInterface $token) {
    $response = $this->requestAccessToken([
      'refresh' => $token->refreshToken(),
    ]);

    if ($response->getStatusCode() === 200) {
      $values = $this->json::decode($response->getBody());
      $values['access_token'] = $values['token'];
      $values['refresh_token'] = 'refresh';
      $values['expires_in'] = $this->config->get('token_expire');
      return new OAuthToken($values['access_token'], $values['refresh_token'], $values['expires_in']);
    }

    throw new RequestOAuthAccessTokenFailed();
  }

  /**
   * Perform a request to the EditionGuard OAuth token exchange.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Full response from the request for an access token.
   */
  protected function requestAccessToken() {

    $response = $this->getHttpClient()
      ->post('https://app.editionguard.com/api/v2/obtain-auth-token', [
        'http_errors' => FALSE,
        'form_params' => [
          'email' => $this->config->get('oauth_email'),
          'password' => $this->config->get('oauth_password'),
        ],
      ]);

    if ($this->config->get('logging_enabled')) {
      $this->logger->info('Attempted access token request. Status - :status', [
        ':status' => $response->getStatusCode(),
      ]);
    }

    return $response;
  }

}
