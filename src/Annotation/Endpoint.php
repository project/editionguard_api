<?php

namespace Drupal\editionguard_api\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\editionguard_api\EndpointPluginInterface;

/**
 * Defines the Endpoint.
 *
 * @Annotation
 */
class Endpoint extends Plugin {

  /**
   * Plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * Plugin name.
   *
   * @var string
   */
  public $name;

  /**
   * Endpoint path as a pattern w/ replaceable values.
   *
   * @var string
   */
  public $path;

  /**
   * Endpoint request method.
   *
   * @var string
   */
  public $method = 'GET';

  /**
   * Endpoint's authorization type: key | oauth.
   *
   * @var string
   */
  public $authType = EndpointPluginInterface::ENDPOINT_AUTH_TYPE_OAUTH;

  /**
   * Endpoint documentation URL.
   *
   * @var string
   */
  public $documentationUrl = '';

  /**
   * Array of query parameter keys that are required for the endpoint.
   *
   * @var array
   */
  public $requiredQueryParams = [];

  /**
   * Array of endpoint default request parameters.
   *
   * @var array
   */
  public $defaultRequestQueryParams = [];

  /**
   * Array of form parameter keys that are required for the endpoint.
   *
   * @var array
   */
  public $requiredFormParams = [];

  /**
   * Array of endpoint default request form parameters.
   *
   * @var array
   */
  public $defaultRequestFormParams = [];

}
